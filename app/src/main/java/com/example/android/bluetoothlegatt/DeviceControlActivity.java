/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.gattAttributes.AllGattCharacteristics;
import com.example.android.bluetoothlegatt.gattAttributes.AllGattServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity implements View.OnClickListener {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView mConnectionState;
    private TextView readDataText;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private EditText writeEditText;
    private ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData;
    private BluetoothGattCharacteristic readViscgoCharacteristic,writeViscgoCharacteristic;
    private TextView notifyTextView;

    private static final int VISCGO_CMD_CONNECTION_STATUS = 68;
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            } else if (BluetoothLeService.ACTION_CHANGED_DATA_AVAILABLE.equals(action)) {
                String notifiedData = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
                notifyTextView.setText(notifiedData);
            }
        }
    };

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        HashMap<String,String> characteristicNameUUID = gattCharacteristicData.get(groupPosition)
                                .get(childPosition);
                        String characteristicName = characteristicNameUUID.get(LIST_NAME);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLeService.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                            Toast.makeText(DeviceControlActivity.this, "Reading "+characteristicName+"...", Toast.LENGTH_SHORT).show();
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(characteristic, true);
                            Toast.makeText(DeviceControlActivity.this, "Enabling notification for  "+characteristicName, Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                    return false;
                }
    };

    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        readDataText.setText("Data:");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        readDataText = findViewById(R.id.tv_read_data);
        Button readButton = findViewById(R.id.btn_read);
        writeEditText = findViewById(R.id.et_write);
        Button writeButton = findViewById(R.id.btn_write);
        notifyTextView = findViewById(R.id.tv_notified_data);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        readButton.setOnClickListener(this);
        writeButton.setOnClickListener(this);

        findViewById(R.id.btn_write_cs).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_read:
                mBluetoothLeService.readCharacteristic(readViscgoCharacteristic);
                break;

            case R.id.btn_write:
                hideKeyboardFrom(DeviceControlActivity.this,writeEditText);
                String number = writeEditText.getText().toString();
                if (number.isEmpty()){
                    Toast.makeText(DeviceControlActivity.this, "Enter number", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (writeViscgoCharacteristic == null){
                    Toast.makeText(DeviceControlActivity.this, "Viscgo write characteristic not found", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Need to check if the string is empty since isDigitsOnly returns
                // true for empty strings.
                if (android.text.TextUtils.isDigitsOnly(number)) {
                    int newBatteryLevel = Integer.parseInt(number);
                    if (newBatteryLevel <= 100) {
                        mBluetoothLeService.writeValueToCharacteristic(writeViscgoCharacteristic,number);
                    } else {
                        Toast.makeText(DeviceControlActivity.this, "Entered number is too high", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(DeviceControlActivity.this, "Enter between 0-100 ", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btn_write_cs:
                if (writeViscgoCharacteristic == null){
                    Toast.makeText(DeviceControlActivity.this, "Viscgo write characteristic not found", Toast.LENGTH_SHORT).show();
                    return;
                }

                mBluetoothLeService.writeToCharacteristic(writeViscgoCharacteristic,getByteMsg(VISCGO_CMD_CONNECTION_STATUS));
                break;
        }
    }

    private byte[] getByteMsg(int cmdType) {
        byte[] msg;
        switch (cmdType){
            case VISCGO_CMD_CONNECTION_STATUS:
                msg = new byte[4];
                msg[0] = 0x68;  /*head*/
                msg[1] = 0x00;  /*Command*/
                msg[2] = 0x00;  /*Length*/

                int checkSum = 0;
                for(byte b : msg){
                    checkSum += (0xff & b);
                }
                String checkSumHex = Integer.toHexString(checkSum & 0xFF);
                Log.d("getByteMsg","Checksum is: "+ checkSumHex);
                msg[3] = (byte) checkSum;  /*Checksum*/
                break;
            default:
                msg = new byte[0];
        }
        return msg;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data) {
        if (data != null) {
            readDataText.setText("Data:"+data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String testServiceString = getResources().getString(R.string.test_service);
        String testCharaString = getResources().getString(R.string.test_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        gattCharacteristicData = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            Log.e("displayGattServices","gattService uuid:"+uuid);
            currentServiceData.put(LIST_NAME, AllGattServices.lookup(uuid, testServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> bluetoothGattCharacteristics = new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                bluetoothGattCharacteristics.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                Log.e("displayGattServices","gattCharacteristic uuid:"+uuid);
                currentCharaData.put(LIST_NAME, AllGattCharacteristics.lookup(uuid, testCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);

                if (gattCharacteristic.getUuid().equals(SampleGattAttributes.VISCGO_WRITE_UUID)) {
                    writeViscgoCharacteristic = gattCharacteristic;
                }

                if (gattCharacteristic.getUuid().equals(SampleGattAttributes.VISCGO_READ_UUID)) {
                    readViscgoCharacteristic = gattCharacteristic;
                }

                StringBuilder properties = new StringBuilder();
                if (isCharacteristicPropertiesAvailable(gattCharacteristic,BluetoothGattCharacteristic.PROPERTY_READ)){
                    properties.append(" ").append("READ");
                }

                if (isCharacteristicPropertiesAvailable(gattCharacteristic,BluetoothGattCharacteristic.PROPERTY_WRITE)){
                    properties.append(" ").append("WRITE");
                }

                if (isCharacteristicPropertiesAvailable(gattCharacteristic,BluetoothGattCharacteristic.PROPERTY_NOTIFY)){
                    properties.append(" ").append("NOTIFY");
                }

                if (isCharacteristicPropertiesAvailable(gattCharacteristic,BluetoothGattCharacteristic.PROPERTY_INDICATE)){
                    properties.append(" ").append("INDICATE");
                }

                Log.e("properties",gattCharacteristic.getUuid()+" "+properties.toString());

                StringBuilder permissions = new StringBuilder();
                if (isCharacteristicPermissionAvailable(gattCharacteristic,BluetoothGattCharacteristic.PERMISSION_READ)){
                    permissions.append(" ").append("READ");
                }

                if (isCharacteristicPermissionAvailable(gattCharacteristic,BluetoothGattCharacteristic.PERMISSION_WRITE)){
                    permissions.append(" ").append("WRITE");
                }

                if (isCharacteristicPermissionAvailable(gattCharacteristic,BluetoothGattCharacteristic.PERMISSION_READ_ENCRYPTED)){
                    permissions.append(" ").append("READ_ENCRYPTED");
                }

                if (isCharacteristicPermissionAvailable(gattCharacteristic,BluetoothGattCharacteristic.PERMISSION_WRITE_ENCRYPTED)){
                    permissions.append(" ").append("WRITE_ENCRYPTED");
                }

                Log.e("permissions",gattCharacteristic.getUuid()+" "+permissions.toString());

            }
            mGattCharacteristics.add(bluetoothGattCharacteristics);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_CHANGED_DATA_AVAILABLE);
        return intentFilter;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private Boolean isCharacteristicPropertiesAvailable(BluetoothGattCharacteristic chara, int propertyType) {
        return ((chara.getProperties() & propertyType) != 0);
    }

    private Boolean isCharacteristicPermissionAvailable(BluetoothGattCharacteristic chara, int permissionType) {
        return ((chara.getPermissions() & permissionType) != 0);
    }
}
