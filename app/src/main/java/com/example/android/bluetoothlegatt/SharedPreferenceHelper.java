package com.example.android.bluetoothlegatt;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferenceHelper {
    private SharedPreferences sharedPreferences;

    public SharedPreferenceHelper(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void firstTimeAskedPermission(String permission, boolean isFirstTime){
        sharedPreferences
                .edit()
                .putBoolean(permission, isFirstTime)
                .apply();
    }

    boolean isFirstTimeAskingPermission(String permission){
        return sharedPreferences.getBoolean(permission,false);
    }
}
